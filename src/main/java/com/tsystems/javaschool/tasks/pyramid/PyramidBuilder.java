package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // npe check
        if (inputNumbers == null)
            throw new CannotBuildPyramidException();
        for (Integer element : inputNumbers) {
            if (element == null)
                throw new CannotBuildPyramidException();
        }

        try {

            // use linked list
            final LinkedList<Integer> list = new LinkedList<>(inputNumbers);

            // sort list
            Collections.sort(list);

            // calculate triangle number
            final double triangle = (Math.sqrt(8 * list.size() + 1) - 1) / 2;

            // not triangle
            if (triangle % 1 != 0)
                throw new CannotBuildPyramidException();

            // calc pyramid props
            final int rows = (int) triangle;
            final int cols = rows + (rows - 1);

            int[][] pyramid = new int[rows][];

            // build y
            int sideZeroCount = (int) (cols / 2);
            for (int y = 0; y < rows; y++) {
                pyramid[y] = new int[cols];

                int xIdx = 0;

                // left zeros
                for (int i = 0; i < sideZeroCount; i++)
                    pyramid[y][xIdx++] = 0;

                // count of elements inside line
                final int elementCount = cols - sideZeroCount * 2;
                for (int i = 1; i <= elementCount; i++)
                    pyramid[y][xIdx++] = i % 2 == 0 ? 0 : list.removeFirst();


                // right zeros
                for (int i = 0; i < sideZeroCount; i++)
                    pyramid[y][xIdx++] = 0;

                // decrease side zero count for next row
                sideZeroCount--;
            }

            return pyramid;
        } catch (OutOfMemoryError oom) {
            throw new CannotBuildPyramidException();
        }
    }
}
