package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        int containsObjects = 0;
        for (Object objA : x) {
            for (int i = 0; i < y.size(); i++) {
                final Object objB = y.remove(0);

                if (Objects.equals(objA, objB)) {
                    containsObjects++;
                    break;
                }
            }
        }

        return containsObjects == x.size();
    }
}
