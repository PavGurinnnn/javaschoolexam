package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    static double calculate(double leftValue, double rightValue, char operator) {
        switch (operator) {
            case '+':
                return leftValue + rightValue;
            case '-':
                return leftValue - rightValue;
            case '*':
                return leftValue * rightValue;
            case '/':
                if (rightValue == 0) return Double.parseDouble(null);
                return leftValue / rightValue;
            case '%':
                return leftValue % rightValue;

            default:
                return 0;
        }
    }


    public String evaluate(String statement) {
        // remove spaces
        try {
            statement = statement.replaceAll("[ |\t]+", "");

            final LinkedList<Double> valueList = new LinkedList<>();
            final LinkedList<Character> operatorList = new LinkedList<>();

            for (int charIndex = 0; charIndex < statement.length(); charIndex++) {
                final char curChar = statement.charAt(charIndex);

                // group started
                if (curChar == '(') {
                    operatorList.add('(');
                }
                // group finished
                else if (curChar == ')') {
                    // calculate group
                    while (operatorList.getLast() != '(') {
                        // get values
                        final double rightValue = valueList.removeLast();
                        final double leftValue = valueList.removeLast();

                        // operator
                        final char operator = operatorList.removeLast();

                        // calculate
                        valueList.add(calculate(leftValue, rightValue, operator));
                    }

                    operatorList.removeLast();
                } else if (isOperator(curChar)) {
                    while (!operatorList.isEmpty() && priority(operatorList.getLast()) >= priority(curChar)) {
                        // get values
                        final double rightValue = valueList.removeLast();
                        final double leftValue = valueList.removeLast();

                        // operator
                        final char operator = operatorList.removeLast();

                        // calculate
                        valueList.add(calculate(leftValue, rightValue, operator));
                    }

                    operatorList.add(curChar);
                } else {
                    // read full value
                    String operand = "";
                    while (charIndex < statement.length() && (Character.isDigit(statement.charAt(charIndex)) || statement.charAt(charIndex) == '.'))
                        operand += statement.charAt(charIndex++);
                    --charIndex;

                    // add value
                    valueList.add(Double.parseDouble(operand));
                }
            }

            // start calculation
            while (!operatorList.isEmpty()) {
                // get values
                final double rightValue = valueList.removeLast();
                final double leftValue = valueList.removeLast();

                // operator
                final char operator = operatorList.removeLast();

                // calculate
                valueList.add(calculate(leftValue, rightValue, operator));
            }


            return prepareResult(valueList.get(0));
        } catch (Exception e) {
            return null;
        }
    }

    private static String prepareResult(double value) {
        String result = String.valueOf(value);

        // remove trailing zeros
        if (result.contains("."))
            result = result.replaceAll("0*$", "");

        // remove dot if it's last char
        result = result.replaceAll("\\.$", "");

        return result;
    }
}
